﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StajyerTakip.Attributes;
using StajyerTakip.Models;


namespace StajyerTakip.Controllers
{
    [GirisKontrol]
    public class GunlukController : Controller
    {
        private readonly Context db;
        public GunlukController(Context db)
        {
            this.db = db;
        }
        [GunlukEkleme]
        public IActionResult Ekle()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Ekle(Models.Gunluk gunluk)
        {
            var id = (int ) HttpContext.Session.GetInt32("id");
            gunluk.OgrenciID = id;
            gunluk.Tarih = DateTime.UtcNow;
            db.Gunlukler.Add(gunluk);
            db.SaveChanges();
            return Redirect("~/Gunluk/Ekle/"+id);


        }
        [GunlukEkleme]
        public IActionResult Duzenle(int id )
        {
            Models.Gunluk gunluk = db.Gunlukler.ToList().Find(x => x.ID == id);


            return View(gunluk);

        }
        [HttpPost]
        public IActionResult Duzenle(Models.Gunluk gunluk, int id)
        {
            Models.Gunluk anaveri = db.Gunlukler.Find(id);

            anaveri.Baslik = gunluk.Baslik;
            anaveri.Bilgiler = gunluk.Bilgiler;
            anaveri.Tarih = gunluk.Tarih;
            


            db.SaveChanges();
            return Redirect("~/Gunluk/Listele");
        }

        [Attributes.StajyerID]
        public ActionResult Listele(int id)
        {
            
            List<Models.Gunluk> gunlukler = db.Gunlukler.ToList().FindAll(x=>x.OgrenciID == id);
            
            foreach(var i in gunlukler)
            {
                i.Ogrenci = db.Stajyerler.Find(i.OgrenciID);
                i.Ogrenci.Profil = db.Hesaplar.Find(i.Ogrenci.ProfilID);
            }
            return View(gunlukler);
        }

        //TODO buraya başka günlükleri görüntülememesi için attribute yazılacak.
        public IActionResult Goruntule(int id)
        {

            Gunluk gunlukler = db.Gunlukler.Find(id);
            
            return View(gunlukler);
        }
    }
}
